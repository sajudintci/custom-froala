import {useState, useEffect} from 'react'
import dynamic from 'next/dynamic'

const FroalaEditor = dynamic(
    () => import('../components/FroalaEditor'),
    {
        ssr: false,
    }
);

function Home() {
  const [text, setText] = useState("")
  const [img, setImg] = useState("")

  function handleModelChange(text) {
    setText(text)
  }

  return <FroalaEditor 
          model={text}
          onModelChange={handleModelChange}
        />
}

export default Home