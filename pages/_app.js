import dynamic from 'next/dynamic'
import '../styles/globals.css'
// Require Editor JS files.
// dynamic(import('froala-editor/js/froala_editor.pkgd.min.js'), { ssr: false })
// dynamic(import('froala-editor/js/plugins.pkgd.min.js'), { ssr: false })
// import 'froala-editor/js/froala_editor.pkgd.min.js'
// import 'froala-editor/js/plugins.pkgd.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'

// Require Font Awesome.
import 'font-awesome/css/font-awesome.css'

// bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
