// Require Editor JS files.
import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/js/plugins.pkgd.min.js';

// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';

// bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

import React, {Component} from 'react'
import FroalaEditor from 'react-froala-wysiwyg';
import Froalaeditor from 'froala-editor';
import $ from 'jquery';
import { Button, Container, Row, Col, Table, Input, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import Image from 'next/image'

const listImage = [
  {id: 1, link: 'https://froala.com/wp-content/uploads/2019/08/medal-2022.svg'},
  {id: 2, link: 'https://akcdn.detik.net.id/community/media/visual/2021/02/23/koran.jpeg'},
  {id: 3, link: 'https://ekopolitan.com/wp-content/uploads/2020/12/contoh-teks-berita-min.jpg'},
  {id: 4, link: 'https://ekopolitan.com/wp-content/uploads/2020/12/struktur-berita-min.jpg'},
  {id: 5, link: 'https://ekopolitan.com/wp-content/uploads/2020/12/ciri-teks-berita-min.jpg'},
  {id: 6, link: 'https://ekopolitan.com/wp-content/uploads/2020/12/syarat-teks-berita-min.jpg'}
]

  // Froalaeditor.DefineIcon('buttonImg', {NAME: 'add-image', SVG_KEY: 'imageManager'});
  // Froalaeditor.RegisterCommand('buttonImg', {
  //   title: 'Insert Image',
  //   focus: true,
  //   undo: true,
  //   refreshAfterCallback: true,
  //   callback: function () {
  //     // document.getElementById('btn-modal').click();
  //     $("#btn-modal" ).click();
  //   }
  // });
const tb = [['undo', 'redo' , 'bold'],['buttonImg']];
class FroalaEditorComponent extends Component {
  constructor(props) {
    super(props);
    this.wrapper = React.createRef();
    this.state = {
      text: '',
      data: [],
      modal: false,
      img: [],
      local: false,
      server: false
    };
    Froalaeditor.DefineIcon('buttonImg', {NAME: 'add-image', SVG_KEY: 'imageManager'});
    Froalaeditor.RegisterCommand('buttonImg', {
      title: 'Insert Image',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
        // document.getElementById('btn-modal').click();
        // $("#btn-modal" ).click();
        // this.wrapper.current.click();
        this.wrapper.current.click()
      }
    });
  }

  toggleModal = () => {
    this.setState({modal: !this.state.modal, server: false, local: false});
  }

  handleFile = (e) => {
    const {img} = this.state
    const content = e.target.result;
    img.push({link: content});
    this.setState({img});
  }

  onChangeImage = (e) => {
    var input = e.target;
    var reader = new FileReader();
    reader.onloadend = this.handleFile;
    reader.readAsDataURL(input.files[0]);
  };

  browseFile = () => {
    document.getElementById('fimg').click();
  }

  selectImageLocal = async () => {
    document.getElementById('list-iamge').value = JSON.stringify(this.state.img);
    this.setState({modal: !this.state.modal});
    document.getElementById('selectImg').click();
  }

  selectImageServer = (item) => {
    const {img} = this.state
    const arrNew = img.filter((b) => b)
    const index = arrNew.findIndex((i) => i.id === item.id);
    if (index < 0) {
      arrNew.push(item);
    } else {
      delete arrNew[index];
    }
    this.setState({img: arrNew});
  }

  checkImg = (item) => {
    const {img} = this.state
    const arrNew = img.filter((b) => b)
    const index = arrNew.findIndex((i) => i.id === item.id);
    return index < 0 ? false : true;
  }

  render() {
    const {update, modal, img, local, server} = this.state
    const {model} = this.props
    const configFloala = {
        placeholderText: 'Edit Your Content Here!',
        charCounterCount: true,
        requestWithCredentials: false,
        requestWithCORS: false,
        language: 'id',
        imageMaxSize: 20 * 1024 * 1024,
        toolbarButtons: tb,
        toolbarButtonsMD: tb,
        toolbarButtonsSM: tb,
        toolbarButtonsXS: tb,
        events: {
          initialized: function () {
            let dom = this;
            document.getElementById('selectImg').addEventListener("click", function(e) {
              const arrImage = JSON.parse(document.getElementById('list-iamge').value);
              for(let item of arrImage) {
                if (item) {
                  dom.image.insert(item.link);
                }
              }
            });
          }
        }
      }
    return (
      <Container>
        <button className="btn-hidde" onClick={this.toggleModal} ref={this.wrapper} id="btn-modal"/>
        <button className="btn-hidde" color="primary" id="selectImg" />
        <input className="btn-hidde" type="textarea" id="list-iamge"/>
        <br/>
        <Row>
          <Col md={12}>
            <FroalaEditor
              tag='textarea'
              config={configFloala}
              model={model}
              onModelChange={this.props.onModelChange}
            />
          </Col>
        </Row>
        <Modal isOpen={modal} size="lg" toggle={this.toggleModal}>
          <ModalHeader>
            Browse File
          </ModalHeader>
          <ModalBody>
            {!local && !server && (
              <center>
                <br/>
                <Button onClick={() => this.setState({local: true, server: false, img: []})}>Browse Local</Button> | 
                <Button onClick={() => this.setState({local: false, server: true, img: []})}>Browse Server</Button>
                <br/><br/><br/>
              </center>
            )}
            { local && (
              <Row>
                <Col md="12">
                  <Row>
                    {img.length > 0 && img.map((item, index) => (
                      <Col key={index.toString()} md="4">
                        <img
                          src={item.link}
                          alt="Picture of the author"
                          width={100}
                          height={100}
                        />
                      </Col>
                    ))}
                  </Row>
                </Col>
                <Col md="12">
                  <br/>
                  <Button onClick={this.browseFile}>Select Image</Button>
                  <Input type="file" className="btn-hidde" id="fimg" accept='image/*' onChange={this.onChangeImage}/>
                </Col>
                <Col md="12">
                  <br/><br/>
                  <Button onClick={this.selectImageLocal} color="primary">Add Image</Button>
                </Col>
              </Row>
            )}

            { server && (
              <Row>
                <Col md="12">
                  <Row>
                    {listImage.length > 0 && listImage.map((item, index) => (
                      <Col key={index.toString()} md="3">
                          <div className={this.checkImg(item) ? 'btn-select' : 'btn-not-select'}>
                            <img
                              src={item.link}
                              alt="Picture of the author"
                              width={150}
                              height={150}
                              onClick={() => this.selectImageServer(item)}
                            />
                          </div>
                      </Col>
                    ))}
                  </Row>
                </Col>
                <Col md="12">
                  <br/><br/>
                  <Button onClick={this.selectImageLocal} color="primary">Add Image</Button>
                </Col>
              </Row>
            )}
          </ModalBody>
        </Modal>
      </Container>
    );
  }
}

export default FroalaEditorComponent;
